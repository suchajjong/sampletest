if [ "$CI_WORKFLOW" = "test build Workflow" ];
then
    echo "Uploading Firebase Debug Build"
    export GOOGLE_APPLICATION_CREDENTIALS=firebase_service_account.json

    echo "FIREBASE_TOKEN: $FIREBASE_TOKEN"
    # Replace My_App_Name & the Firebase App Id (1:1234567890:ios:0a1b2c3d4e5f67890) with your apps information.
    # Add your FIREBASE_TOKEN to your environment variables in your workflow
    # ./firebase-tools-macos appdistribution:distribute $CI_AD_HOC_SIGNED_APP_PATH/SampleTest --app app-1-1069811235348-ios-8cd7760eb22ae656ddc501 --token "1//0ghxi6QW-YdpDCgYIARAAGBASNwF-L9IrhM7YsfGOpnsvsuHqpkBXey87xVvSETu1H3AzTcxoNEVfMwfcP0L5glg3MMsIJlSLQXU" --groups "test"
    ./firebase-tools-macos appdistribution:distribute $CI_DEVELOPMENT_SIGNED_APP_PATH/SampleTest.ipa --app 1:1069811235348:ios:8cd7760eb22ae656ddc501 --groups "test"

fi

if [ "$CI_WORKFLOW" = "test build Workflow" ];
then
    echo "Uploading Tag"
    cd ..
    git remote set-url origin https://oauth2:Axn_NaDBUsysHmU53Mwk@gitlab.com/suchajjong/sampletest.git
    APP_VERSION=$(xcodebuild -scheme SampleTest -project SampleTest.xcodeproj -showBuildSettings | grep "MARKETING_VERSION" | sed 's/[ ]*MARKETING_VERSION = //')

    tag="v.$APP_VERSION($CI_BUILD_NUMBER)"

    echo "$tag"
    git tag $tag

    # Create a personal use github token with read/write access & add it to your Xcode Cloud environment variables.
    git push origin $tag
fi

if [ "$CI_WORKFLOW" = "test production Workflow" ];
then
    echo "Uploading Tag"
    cd ..
    git remote set-url origin https://oauth2:Axn_NaDBUsysHmU53Mwk@gitlab.com/suchajjong/sampletest.git
    APP_VERSION=$(xcodebuild -scheme SampleTest -project SampleTest.xcodeproj -showBuildSettings | grep "MARKETING_VERSION" | sed 's/[ ]*MARKETING_VERSION = //')

    tag="v.$APP_VERSION($CI_BUILD_NUMBER)"

    echo "$tag"
    git tag $tag

    # Create a personal use github token with read/write access & add it to your Xcode Cloud environment variables.
    git push origin $tag
fi
# then
#     echo "Uploading Tag"
#     cd ..
#     APP_VERSION=$(xcodebuild -scheme SampleTest -project SampleTest.xcodeproj -showBuildSettings | grep "MARKETING_VERSION" | sed 's/[ ]*MARKETING_VERSION = //')

#     git tag v$APP_VERSION.$CI_BUILD_NUMBER
#     # Create a personal use github token with read/write access & add it to your Xcode Cloud environment variables.
#     git remote set-url origin https://your_github_username:$GITHUB_TOKEN@github.com/path/to/repository.git
#     git push --tags
# fi