//
//  ViewController.swift
//  SampleTest
//
//  Created by Jong on 7/6/2564 BE.
//

import UIKit
import FirebaseAnalytics
import FirebaseRemoteConfig

class ViewController: UIViewController {

    @IBOutlet weak var btn: UILabel?
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var textLabel2: UILabel?

    // onCreare
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view1: viewDidLoad")
        // Do any additional setup after loading the view.
        for family: String in UIFont.familyNames
               {
                   print(family)
                   for names: String in UIFont.fontNames(forFamilyName: family)
                   {
                       print("== \(names)")
                   }
               }
        textLabel?.font = UIFont(name: "FontAwesome5Pro-Regular", size: CGFloat(16))
        textLabel?.text = ""
        setConfig()
//        fatalError("Crash was triggered")
    }
    
    //onStart
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("view1: viewWillAppear")

    }
    
    //onResume
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view1: viewDidAppear")
        Analytics.logEvent("test_user_2_ios_iiu1_iip5", parameters: [
                                    "items": "4"])
        
    }
    
    
    //onPause
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("view1: viewWillDisappear")

    }
    
    //onstop
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view1: viewDidDisappear")

    }
    
    @IBAction func presentDetail(_ sender: Any) {
//        self.presentDetail()
//        Analytics.logEvent("test_price_2", parameters: [
//                            "test_custom_item" : "i1",
//                            "c_price" : 1])
//        Analytics.logEvent("test_price_curr_ios_3", parameters: [
//                            "c_price" : 1,
//                            AnalyticsParameterCurrency:"THB"]
        
//        Analytics.setUserID("iiu1")
//        Analytics.setUserProperty("iip5", forName: "user_province")
//        Analytics.logEvent("test_user_2_ios_iiu1_iip5", parameters: [
//                                    "items": "4"])
        
        print(1)
        print(2)
        print(3)
        print(4)
        print(5)
        print(6)

        print("tesddsfdsffdsfdst")
        print("sdfdsfsdf")
        print("dsdsfdsfsdffsdf")
        print("dssdfsdffs")
        
        var a = [1,2]
        a.append(2)
        a.append(3)
        a.append(4)
        a.append(5)
        a.append(6)
        a.append(6)
        a.append(6)
        a.append(6)
        print(a[17])

//        fatalError("Crash was triggered43")

    }
    
    func setConfig(){
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
//        remoteConfig.fetch { (status, error) -> Void in
//          if status == .success {
//            print("Config fetched!")
//            remoteConfig.activate { changed, error in
//
//            }
//          } else {
//            print("Config not fetched")
//            print("Error: \(error?.localizedDescription ?? "No error available.")")
//          }
//        }
        remoteConfig.fetchAndActivate { (status, error) in
            if status == .successFetchedFromRemote{
                print("Config fetched!")
            }
            self.textLabel2?.text = remoteConfig.configValue(forKey: "price").numberValue.stringValue
            let feature_a = remoteConfig.configValue(forKey: "feature_test_a").boolValue
            
            if feature_a {
                self.textLabel2?.text = "true"
            }
            else {self.textLabel2?.text = "false"}
            
        }
    }
    
   

}

extension UIViewController{
    func presentDetail(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "detailView") as! DetailView
        
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .coverVertical
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
