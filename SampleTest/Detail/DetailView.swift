//
//  DetailView.swift
//  SampleTest
//
//  Created by Jong on 16/6/2564 BE.
//

import UIKit

class DetailView: UIViewController {

    @IBOutlet weak var btn: UILabel?
    // onCreare
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view2: viewDidLoad")
        // Do any additional setup after loading the view.
    }
    
    //onStart
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("view2: viewWillAppear")
    }
    
    //onResume
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view2: viewDidAppear")

        

    }
    
    
    //onPause
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("view2: viewWillDisappear")

    }
    
    //onstop
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view2: viewDidDisappear")

    }
    
    

}
